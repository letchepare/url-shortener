# Use the official Node.js 14 image as a base
FROM node:20.15-alpine

# Set the working directory inside the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to WORKDIR
COPY package*.json ./

# Install dependencies (use --only=production for production image)
RUN npm install --only=production
COPY ./prisma ./prisma
RUN npx prisma generate

# Copy the rest of the application code to WORKDIR
COPY . .

# Expose the port the app runs on
EXPOSE 3000

# Command to run the application
CMD ["node", "server.js"]

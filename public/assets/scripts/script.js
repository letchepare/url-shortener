document.getElementById("url-form").addEventListener("submit", function (e) {
  e.preventDefault();

  const longUrl = document.getElementById("long-url").value;
  const resultDiv = document.getElementById("result");
  const shortUrlLink = document.getElementById("short-url");

  fetch("/shorten", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ longUrl }),
  })
    .then((response) => response.json())
    .then((data) => {
      const shortUrl = `${window.location.origin}/${data.shortUrl}`;
      shortUrlLink.href = shortUrl;
      shortUrlLink.textContent = shortUrl;
      resultDiv.classList.remove("hidden");
    })
    .catch((error) => {
      console.error("Error:", error);
    });
});

document
  .getElementById("language-select")
  .addEventListener("change", function () {
    const selectedLanguage = this.value;
    localStorage.setItem("preferredLanguage", selectedLanguage);
    translatePage(selectedLanguage);
  });

function loadPreferredLanguage() {
  const savedLanguage = localStorage.getItem("preferredLanguage") || "en";
  document.getElementById("language-select").value = savedLanguage;
  translatePage(savedLanguage);
}

// Set default language
loadPreferredLanguage();

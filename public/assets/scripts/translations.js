const translations = {
  en: {
    title: "URL Shortener",
    "placeholder-url": "Enter the URL you want to shorten",
    "shorten-button": "Shorten",
    "shortened-url": "Shortened URL:",
    "select-language": "Select Language:",
  },
  es: {
    title: "Acortador de URL",
    "placeholder-url": "Ingrese su URL larga",
    "shorten-button": "Acortar",
    "shortened-url": "URL acortada:",
    "select-language": "Seleccionar idioma:",
  },
  fr: {
    title: "Raccourcisseur d'URL",
    "placeholder-url": "Entrez votre URL à raccourcir",
    "shorten-button": "Raccourcir",
    "shortened-url": "URL raccourcie:",
    "select-language": "Choisir la langue:",
  },
};

function translatePage(language) {
  document.querySelector("html").setAttribute("lang", language);
  document.querySelectorAll("[data-translate]").forEach((element) => {
    const key = element.getAttribute("data-translate");
    element.textContent = translations[language][key];
  });
  const urlInput = document.getElementById("long-url");
  urlInput.placeholder = translations[language]["placeholder-url"];
}

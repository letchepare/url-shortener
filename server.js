require('dotenv').config();

const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const shortid = require("shortid");
const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();
const app = express();

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

app.post("/shorten", async (req, res) => {
  const longUrl = req.body.longUrl;
  const shortUrl = shortid.generate();
  const url = await prisma.url.create({
    data: {
      shortUrl,
      longUrl,
    },
  });
  res.json({ shortUrl: url.shortUrl });
});

app.get("/:shortUrl", async (req, res) => {
  const url = await prisma.url.findUnique({
    where: {
      shortUrl: req.params.shortUrl,
    },
  });
  if (url) {
    res.redirect(url.longUrl);
  } else {
    res.status(404).send("URL not found");
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
